from bson.objectid import ObjectId  # noqa
import sys

sys.path.append("..")
from car.databases.client import MongoLsv

mongo_lsv = MongoLsv()
print(mongo_lsv.list_dbs())
print(mongo_lsv.list_collections(db_name="genero_de_peliculas"))

print(
    mongo_lsv.delete_record_in_collection(
        db_name="genero_de_peliculas",
        collection="peliculas",
        record_id="62acc1c9b3db0ce51868e24b", #filled from de db
    )
)

print(
    mongo_lsv.get_records_from_collection(
        db_name="genero_de_peliculas", collection="peliculas"
    )
)