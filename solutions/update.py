from bson.objectid import ObjectId  # noqa
from car.databases.client import MongoLsv

mongo_lsv = MongoLsv()
print(mongo_lsv.list_dbs())
print(mongo_lsv.list_collections(db_name="genero_de_peliculas"))

print(
    mongo_lsv.update_record_in_collection(
        db_name="genero_de_peliculas",
        collection="peliculas",
        record_query={"_id": ObjectId("62acc1c9b3db0ce51868e24b")},
        record_new_value={"label": "pirates of the caribean"},
    )
)

print(
    mongo_lsv.get_records_from_collection(
        db_name="genero_de_peliculas", collection="peliculas"
    )
)