from bson.objectid import ObjectId  # noqa
import sys

print(sys.path.append('..'))
from car.databases.client import MongoLsv

mongo_lsv = MongoLsv()
print(mongo_lsv.list_dbs())
print(mongo_lsv.list_collections(db_name="genero_de_peliculas"))


print(
    mongo_lsv.create_new_record_in_collection(
        db_name="genero_de_peliculas",
        collection="peliculas",
        record={"label": "piratas del caribe", "value": "aventura"},
    )
)

print(
    mongo_lsv.get_records_from_collection(
        db_name="genero_de_peliculas", collection="peliculas"
    )
)